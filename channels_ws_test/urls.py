from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^chats/', include('chat.urls')),
    url(r'^channels-api/', include('channels_api.urls'))
]
