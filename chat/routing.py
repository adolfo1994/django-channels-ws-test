from channels.generic.websockets import WebsocketDemultiplexer
from channels.routing import route_class, route
from .bindings import RoomBinding, MessageBinding


class APIDemultiplexer(WebsocketDemultiplexer):

    consumers = {
        'rooms': RoomBinding.consumer,
        'messages': MessageBinding.consumer
    }


channel_routing = [
    route_class(APIDemultiplexer)
]
