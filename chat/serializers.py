from rest_framework import serializers
from .models import Message, Room


class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ('content', )


class RoomSerializer(serializers.ModelSerializer):

    messages = MessageSerializer(many=True, required=False)

    class Meta:
        model = Room
        fields = ('name', 'label', 'messages')
        read_only_fields = ('messages',)
