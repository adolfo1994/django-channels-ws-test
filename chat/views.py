from rest_framework import viewsets
from .models import Room, Message
from .serializers import RoomSerializer, MessageSerializer
from django.shortcuts import render, redirect
import haikunator
from django.db import transaction

haiku = haikunator.Haikunator()


class RoomViewset(viewsets.ModelViewSet):

    serializer_class = RoomSerializer
    queryset = Room.objects.all()


class MessageViewset(viewsets.ModelViewSet):

    serializer_class = MessageSerializer
    queryset = Message.objects.all()


def chat_room(request, label):
    room, created = Room.objects.get_or_create(label=label)

    messages = reversed(room.messages.order_by('-timestamp')[:50])

    return render(request, "chat/room.html", {
        'room': room,
        'messages': messages,
    })


def new_room(request):
    """
    Randomly create a new room, and redirect to it.
    """
    new_room = None
    while not new_room:
        with transaction.atomic():
            label = haiku.haikunate()
            if Room.objects.filter(label=label).exists():
                continue
            new_room = Room.objects.create(label=label)
    return redirect(chat_room, label=label)


def about(request):
    return render(request, "chat/about.html")
