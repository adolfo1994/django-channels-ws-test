from django.db import models
import datetime


class Room(models.Model):

    name = models.CharField(max_length=64)
    label = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class Message(models.Model):

    room = models.ForeignKey(Room, related_name='messages')
    content = models.CharField(max_length=64)
    handle = models.TextField()
    timestamp = models.DateTimeField(default=datetime.datetime.now, db_index=True)

    def __str__(self):
        return "{} {}".format(self.room.name, self.content)
