from channels_api.bindings import ResourceBinding

from .models import Room, Message
from .serializers import RoomSerializer, MessageSerializer


class RoomBinding(ResourceBinding):

    model = Room
    stream = 'rooms'
    serializer_class = RoomSerializer
    queryset = Room.objects.all()


class MessageBinding(ResourceBinding):

    model = Message
    stream = 'messages'
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
