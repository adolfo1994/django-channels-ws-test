from rest_framework.routers import DefaultRouter
from .views import RoomViewset, MessageViewset, chat_room, about, new_room
from django.conf.urls import url

router = DefaultRouter()
router.register(r'rooms', RoomViewset)
router.register(r'messages', MessageViewset)
urlpatterns = (
    router.urls +
    [
        url(r'^$', about, name='about'),
        url(r'^new/$', new_room, name='new_room'),
        url(r'^(?P<label>[\w-]{,50})/$', chat_room, name='chat_room'),

    ]
)
